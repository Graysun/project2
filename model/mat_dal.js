var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.GetAllRaw = function(callback) {
    connection.query('SELECT * FROM rawMats',
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            console.log(result);
            callback(false, result);
        }
    );
}

exports.GetAllRefined = function(callback) {
    connection.query('SELECT * FROM refinedMats',
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            console.log(result);
            callback(false, result);
        }
    );
}

exports.GetAllRefined = function(callback) {
    connection.query('SELECT * FROM refinedMats',
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            console.log(result);
            callback(false, result);
        }
    );
}
exports.RefinedInto = function(rawMat, callback) {
    connection.query("SELECT * FROM raw_to_refined WHERE rawMat= '"+rawMat+"'",
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            console.log(result);
            callback(false, result);
        }
    );
}
