var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.GetAll = function(callback) {
    connection.query('SELECT * FROM ships',
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            console.log(result);
            callback(false, result);
        }
    );
}

exports.GetEquipment = function(callback) {
    connection.query('SELECT * FROM miners',
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            console.log(result);
            callback(false, result);
        }
    );
}


exports.Insert = function(ship, shipType, cargo, speed, shield, armor, structure, callback) {
    var values = [ship, shipType, cargo, speed, shield, armor, structure];
    connection.query('INSERT INTO ships (ship, shipType, cargo, speed, shield, armor, structure) VALUES (?, ?)', values,
        function (err, result) {

            if (err) {
                callback(err);
            }
            else {
                callback(result);
            }
        });
}