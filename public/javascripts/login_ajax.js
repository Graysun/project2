var UserLogin = function() {

    /*
     We define a payload JavaScript object that contains the same sort of information our query parameter string did
     before; i.e. state: $(‘state_name’).val() is the same as ?state_name=user_entered_value_in_the_state_name_input_box
     */
    var payload = {
        email: $('#email').val(),
        password: $('#password').val()
    };


    // Next we configure the jQuery ajax call
    $.ajax({
        url: '/authenticate',  // url where we want to send the form data
        type: 'POST', // the type of form submission; GET or POST
        contentType: "json",  // the type of data we are sending
        data: payload,  // the actual data we are sending
        complete: function(data) {  // what to do with the response back from the server
            window.location.assign('/');
            // take the response from the server and add the information between the <div></div> tags
            //$('#message').html(data.responseJSON.message);
            // show the <div>
            //$('#message').show();
        }
    })
}

$(document).ready(function() {
    console.log('login button clicked');
    // Now that the HTML has been parsed the browser knows that there is a button with the id addBtn.
    // We are overriding the click function of the button, to run the code we specify.
    $('#signin').click(function(e) {
        // When ddBtn is clicked this console log statement logs to your browser's console log not Node.js in Webstorm
        console.log('login button clicked');

        // this prevents the form from being submitted using the non-ajax method
        e.preventDefault();

        // runs the ajax function defined above.
        UserLogin();
    });
});

