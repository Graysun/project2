var express = require('express');
var router = express.Router();
var userDal = require('../model/user_dal');

router.get('/', function(req, res, next) {
  var data = {
    title : 'Express'
  };
  if(req.session.user === undefined) {
    res.render('index', data);
  }
  else {
    data.firstname = req.session.user.firstname;
    res.render('index', data);
  }
});

router.get('/about', function(req, res, next) {
  res.render('about.ejs');
});

router.get('/authenticate', function(req, res) {
  userDal.GetByEmail(req.query.email, req.query.password, function (err, user) {
    if (err) {
      res.send(err);
    }
    else if (user == null) {
      res.send("User not found.");
    }
    else {
      req.session.user = user;
      if(req.session.originalUrl = '/login'){
        req.session.originalUrl = '/';
      }
      res.redirect(req.session.originalUrl);
      //res.send(user);
    }
  });
});

router.get('/login', function(req, res) {
  if(req.session.user) {
    res.redirect('/'); //user already logged in so send them to the homepage.
  }
  else {
    res.render('authentication/login.ejs');
  }
});

router.get('/logout', function(req, res) {
  req.session.destroy( function(err) {
    res.render('authentication/logout.ejs');
  });
});


module.exports = router;
