var express = require('express');
var router = express.Router();
var matDal = require('../model/mat_dal');


router.get('/raw', function(req, res) {
    matDal.GetAllRaw(function (err, result) {
            if (err) {
                res.send("Error" + err.message);
            }
            else {
                res.render('mats/allMats.ejs', {rs: result});
            }
        }
    );
});

router.get('/refined', function(req, res) {
    matDal.GetAllRefined(function (err, result) {
            if (err) {
                res.send("Error" + err.message);
            }
            else {
                res.render('mats/allRefined.ejs', {rs: result});
            }
        }
    );
});

router.get('/raw/refined', function(req, res){
    matDal.RefinedInto(req.query.rawMat, function(err, mat_result){
        if(err) {
            console.log(err);
            res.send('error: ' + err);
        }
        else {
            console.log(mat_result);
            res.render('mats/raw2refine.ejs', {rs: mat_result, matID: req.query.rawMat});
        }
    });
});

module.exports = router;