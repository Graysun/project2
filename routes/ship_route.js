var express = require('express');
var router = express.Router();
var shipDal = require('../model/ship_dal');

router.get('/', function(req, res) {
    shipDal.GetAll(function (err, result) {
            if (err) {
                res.send("Error" + err.message);
            }
            else {
                res.render('ships/allShips.ejs', {rs: result});
            }
        }
    );
});


router.get('/equipment', function(req, res) {
    shipDal.GetEquipment(function (err, result) {
            if (err) {
                res.send("Error" + err.message);
            }
            else {
                res.render('ships/allEquips.ejs', {rs: result});
            }
        }
    );
});

router.post('/insert', function(req, res) {
    console.log(req.body);
    shipDal.Insert(req.body.ship, req.body.shipType, req.body.cargo,
        req.body.speed, req.body.shield, req.body.armor, req.body.structure,
        function(err){
            if(err){
                res.send('Fail!<br />' + err);
            } else {
                res.send('Success!')
            }
        });
});

router.get('/new', function(req, res) {
    shipDal.GetAll( function(err, result){
        if(err) {
            res.send("Error: " + err);
        }
        else {
            res.render('ships/newShip.ejs', {ships: result});
        }
    });

});


module.exports = router;