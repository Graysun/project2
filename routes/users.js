var express = require('express');
var router = express.Router();
var userDal = require('../model/user_dal');

/* GET users listing. */

router.get('/all', function(req, res) {
    userDal.GetAll(function (err, result) {
            if (err) throw err;
            res.render('users/displayAllUsers.ejs', {rs: result});
        }
    );
});


router.get('/', function (req, res) {
    userDal.GetByRatings(req.query.userID, function (err, result) {
            if (err) throw err;

            res.render('users/displayUserInfo.ejs', {rs: result, userID: req.query.userID});
        }
    );
});

router.get('/create', function (req, res, next) {
    res.render('users/userFormCreate.ejs');
})

router.get('/new', function(req, res) {
    userDal.GetState( function(err, result){
        if(err) {
            res.send("Error: " + err);
        }
        else {
            res.render('users/userFormCreate.ejs', {state: result});
        }
    });

});

router.get('/save', function(req, res, next) {
    console.log("firstname equals: " + req.query.firstname);
    console.log("lastname equals: " + req.query.lastname);
    console.log("email equals: " + req.query.email);
    console.log("password equals: " + req.query.password);
    console.log("state equals: " + req.query.state_id);
    userDal.Insert(req.query, function (err, result) {
        if (err) {
            res.send(err);
        }
        else {
            res.send("Successfully received request.");
        }
    });
});


router.get('/edit', function(req, res){
    console.log('/edit userID:' + req.query.userID);

    userDal.GetByID(req.query.userID, function(err, users_result){
        if(err) {
            console.log(err);
            res.send('error: ' + err);
        }
        else {
            console.log(users_result);
            userDal.GetState(function(err, state_result){
                console.log(state_result);
                res.render('users/userFormEdit.ejs', {rs: users_result, state: state_result, message: req.query.message});
            });
        }
    });
});

// Added for Lab 10

router.post('/update_users', function(req, res){
    console.log(req.body);
    userDal.Update(req.body.userID, req.body.firstname, req.body.lastname, req.body.state_id, req.body.email, req.body.password,
        function(err){
            console.log("finished with ")
            var message;
            if(err) {
                console.log("error: " + err);
                message = 'error: ' + err.message;
            }
            else {
                message = 'success';
                console.log(message);
            }

            console.log("Outside of if else condition in update Users ");
            userDal.GetByID(req.body.userID, function(err, user_info){
                userDal.GetState(function(err, state_result){
                    console.log("In GetState function");
                    console.log(state_result);
                    res.redirect('/users/edit/?userID=' + req.body.userID + '&message=' + message);
                    //res.render('movie/movie_edit_form', {rs: movie_info, genres: genre_result});
                });
            });


        });
});

// Added for Lab 10

router.get('/delete', function(req, res){
    console.log(req.query);
    userDal.GetByID(req.query.userID, function(err, result) {
        if(err){
            res.send("Error: " + err);
        }
        else if(result.length != 0) {
            userDal.DeleteById(req.query.userID, function (err) {
                res.send(result[0].firstname + ' Successfully Deleted');
            });
        }
        else {
            res.send('User does not exist in the database.');
        }
    });
});

router.get('/users_insert', function(req, res){
    console.log(req.body);
    userDal.Insert(req.query.firstname, req.query.lastname, req.query.state_id, req.query.email, req.query.password, function(err, result){
        var response = {};
        if(err) {
            response.message = err.message;
        }
        else {
            response.message = 'Success!';
        }

    });
});


module.exports = router;
